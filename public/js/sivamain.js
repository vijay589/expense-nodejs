$(document).ready(function() {
    function populateTable() {
        $.ajax({
            type: 'GET',
            url: '/api/expenses',
            success: function(expenses1) {
                var tablerow = '';
                $.each(expenses1, function() {
                    console.log(this._id);
                    console.log(this.date);
                    console.log(this.category);
                    console.log(this.amount);
                    console.log(this.description);
                    tablerow += '<tr data-id=' + this._id + '>';
                    tablerow += '<td> <span class="noedit description">' + this.description + ' </span><input class="edit description"/></td>';
                    tablerow += '<td> <span class="noedit amount">' + this.amount + ' </span><input class="edit amount"/></td>';
                    tablerow += '<td> <span class="noedit category">' + this.category + ' </span><input list="categories"class="edit category"/><datalist id="categories"><option value="C01:Home"><option value="C02:Groceries"><option value="C03:Personal Care"><option value="C04:Telecommunication"><option value="C05:Automobiles"><option value="C06:Pocket Money"><option value="C07:Travel"><option value="C08:Entertainment/Eatout"><option value="C09:Stationary"><option value="C10:People"><option value="C11:Medical"><option value="C12:Debit/Emis"><option value="C13:Tips/Dharman"><option value="C14:Saving/Future"><option value="C15:One Time"></datalist></td>';
                    tablerow += '<td> <span class="noedit date">' + this.date + '</span><input type="text" id="datepicker" class="edit date"/></td>';
                    tablerow += '<td style="width:20%;"><img src="./images/Edit.png" class="editExpense noedit"/><img src="./images/Save.png" class="saveExpense edit"/><img src="./images/DeleteRed1.png" class=" cancelExpense edit"/><img src="./images/DeleteRed1.png" class="linkdeleteExpense" data-id="' + this._id + '"/></td>';
                    $('#expenseList table tbody ').html(tablerow);
                });
            }
        });
    };
    $('thead th').each(function(column) {
        $(this).addClass('sortable').click(function() {
            var findSortKey = function($cell) {
                return $cell.find('.sort-key').text().toUpperCase() + ' ' + $cell.text().toUpperCase();
            };
            var sortDirection = $(this).is('.sorted-dsc') ? 1 : -1;
            var $rows = $(this).parent().parent().parent().find('tbody tr').get();

            //loop through all the rows and find 
            $.each($rows, function(index, row) {
                row.sortKey = findSortKey($(row).children('td').eq(column));
            });
            $rows.sort(function(a, b) {
                if (a.sortKey < b.sortKey) return -sortDirection;
                if (a.sortKey > b.sortKey) return sortDirection;
                return 0;
            });
            $.each($rows, function(index, row) {
                $('tbody').append(row);
                row.sortKey = null;
            });
        });
    });

    $(function() {
        $("#dialog").dialog();
    });
    $('#search').keyup(function() {
        searchTable($(this).val());
    });
    populateTable();
    $("#expenseList").delegate('.editExpense', 'click', function() {
        var $tr = $(this).closest('tr');
        $tr.find('input.description').val($tr.find('span.description').html());
        $tr.find('input.amount').val($tr.find('span.amount').html());
        $tr.find('input.category').val($tr.find('span.category').html());
        $tr.find('input.date').val($tr.find('span.date').html());
        $tr.addClass('edit');
    });
    $("#expenseList").delegate('.cancelExpense', 'click', function() {
        $(this).closest('tr').removeClass('edit');
    });
    $("#expenseList").delegate('.saveExpense', 'click', function() {
        var $tr = $(this).closest('tr');
        var $desc = $('#desc');
        var $amount = $('#amount');
        var $category = $('#category');
        var $date = $('#date');
        var expense = {
            description: $tr.find('input.description').val(),
            amount: $tr.find('input.amount').val(),
            category: $tr.find('input.category').val(),
            date: $tr.find('input.date').val(),
        };
        $.ajax({
            type: 'PUT',
            url: '/api/expenses/' + $tr.attr('data-id'),
            data: expense,
            success: function(expenses1) {
                $tr.find('span.description').html(expense.description);
                $tr.find('span.amount').html(expense.amount);
                $tr.find('span.category').html(expense.category);
                $tr.find('span.date').html(expense.date);
                $tr.removeClass('edit');
                alert('expense updated!!');
            },
        });
    });
    $("#expenseList").delegate('.linkdeleteExpense', 'click', function() {
        var $tr = $(this).closest('tr');
        var confirmation = confirm('Are you sure you want to delete this user ?' + $(this).attr('data-id'));
        if (confirmation === true) {
            $.ajax({
                type: 'DELETE',
                url: '/api/expenses/' + $(this).attr('data-id')
            }).done(function(response) {
                $tr.fadeOut(300, function() {
                    $(this).remove();
                });
            });
        } else {
            return false;
        }
    });
});

function searchTable(inputVal) {
    var table = $('#tblData1');
    table.find('tr').each(function(index, row) {
        var allCells = $(row).find('td');
        if (allCells.length > 0) {
            var found = false;
            allCells.each(function(index, td) {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text())) {
                    found = true;
                    return false;
                }
            });
            if (found == true) $(row).show();
            else $(row).hide();
        }
    });
}
$(function() {
    $("#datepicker").datepicker({
        dateFormat: "dd-mm-yy"
    }).val()
});
$(function() {
    var $desc = $('#desc');
    var $amount = $('#amount');
    var $category = $('#category');
    var $date = $('#date');
    $('#save').on('click', function() {
        var expense = {
            description: $desc.val(),
            amount: $amount.val(),
            category: $category.val(),
            date: $date.val(),
        };
        $.ajax({
            type: 'POST',
            url: '/api/expenses',
            data: expense,
            success: function(newExpense) {
                alert('expense addedd !!');
            }
        });
    });
});