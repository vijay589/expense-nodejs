// app/models/expense.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ExpenseSchema   = new Schema({
    description: String,
    amount: String,
    category: String,
    date: { type: Date, default: Date.now }

});

module.exports = mongoose.model('Expense', ExpenseSchema);