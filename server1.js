//server1.js
var express    = require('express');        
var app        = express();  
var bodyParser = require('body-parser');

var mongoose   = require('mongoose');
mongoose.connect('localhost:27017'); 


var Expense    = require('./app/models/expense'); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080; 
var util = require('util');

var static_router = express.Router();
static_router.get('/', function(req, res) {
    res.json({ message: 'You ANR!' });   
});

static_router.get('/apr', function(req, res) {
    res.json({ message: 'You are APR!' });   
});

//router api
var router = express.Router();
router.use(function(req, res, next) {
    console.log('Something is happening.');
    next(); 
});

router.get('/', function(req, res) {
    res.json({ message: 'Hi! welcome to  Expense api!' });   
});

router.route('/expenses')
    .post(function(req, res) {
        
        var expense = new Expense();      
        expense.description = req.body.description;
        expense.amount =req.body.amount;
        expense.category =req.body.category;
        if (req.body.date != null) {
          expense.date = req.body.date;    
        }

        expense.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Expense created!' });
        });
        
    })
     
    .get(function(req, res) {
        Expense.find(function(err, expenses) {
            if (err)
                res.send(err);

            res.json(expenses);
        });
    });
    // on routes that end in /bears/:bear_id
// ----------------------------------------------------
router.route('/expenses/:expense_id')

    // get the bear with that id (accessed at GET http://localhost:8080/api/bears/:bear_id)
    .get(function(req, res) {
        Expense.findById(req.params.expense_id, function(err, expense) {
            if (err)
                res.send(err);
            res.json(expense);
        });
    })
       // update the bear with this id (accessed at PUT http://localhost:8080/api/bears/:bear_id)
    .put(function(req, res) {

        // use our bear model to find the bear we want
        Expense.findById(req.params.expense_id, function(err, expense) {

            if (err)
                res.send(err);

                expense.description = req.body.description;
                expense.amount =req.body.amount;
                expense.category =req.body.category;
                expense.date =req.body.date;  // update the bears info

            // save the bear
            expense.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Expense updated!' });
            });

        });
    })
        // delete the bear with this id (accessed at DELETE http://localhost:8080/api/bears/:bear_id)
    .delete(function(req, res) {
        Expense.remove({
            _id: req.params.expense_id

           

        }, function(err, expense) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

app.use('/api', router);
app.use('/', static_router);

app.use(express.static('public'));

app.listen(port);
console.log('Magic happens on port ' + port);
